#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 00:07:25 2018

@author: eslam
"""

#importing the libraries 
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# importing the dataset
dataset = pd.read_csv("Churn_Modelling.csv")
X = dataset.iloc[:, 3:13].values
y = dataset.iloc[:,13].values

# Encoding categorical data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

labelencoder_x1 = LabelEncoder()
X[:, 1] = labelencoder_x1.fit_transform(X[:, 1])

labelencoder_x2 = LabelEncoder()
X[:, 2] = labelencoder_x1.fit_transform(X[:, 2])

onehotencoder = OneHotEncoder(categorical_features = [1])
X = onehotencoder.fit_transform(X).toarray()
X = X[:, 1:]

# Spliting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)


from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()

# Fit only to the Taining set
scaler.fit(X_train)

# Scaling data
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

# Using Neural Network(NN) 
from sklearn.neural_network import MLPClassifier
mlp = MLPClassifier(hidden_layer_sizes=(30,30,30))
mlp.fit(X_train, y_train)

# prediction
predictions = mlp.predict(X_test)

# Model report and Accuracy
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

classificationReport = classification_report(y_test, predictions) 
print(classificationReport)

confusionMatrix = confusion_matrix(y_test, predictions)
print(confusionMatrix)

accuracy = accuracy_score(y_test, predictions)
print("Total accuracy= ", accuracy)


