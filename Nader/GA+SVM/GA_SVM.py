import numpy
import sys
sys.path.append("..\..")
from ipynb.fs.full.data_preprocessing import *
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix


def cal_pop_fitness(pop):
    # Calculating the fitness value of each solution in the current population.
    # The fitness function calulates the sum of products between each input and its corresponding weight.
    fitness=[]
    for i in range(len(pop)):
        fitness.append(getaccuracy(abs(pop[i][0]),abs(pop[i][1])))
    fitness_np = numpy.array(fitness)
    return fitness_np

def select_mating_pool(pop, fitness, num_parents):
    # Selecting the best individuals in the current generation as parents for producing the offspring of the next generation.
    parents = numpy.empty((num_parents, pop.shape[1]))
    for parent_num in range(num_parents):
        max_fitness_idx = numpy.where(fitness == numpy.max(fitness))
        max_fitness_idx = max_fitness_idx[0][0]
        parents[parent_num, :] = pop[max_fitness_idx, :]
        fitness[max_fitness_idx] = -99999999999
    return parents

def crossover(parents, offspring_size):
    offspring = numpy.empty(offspring_size)
    # The point at which crossover takes place between two parents. Usually, it is at the center.
    crossover_point = numpy.uint8(offspring_size[1]/2)

    for k in range(offspring_size[0]):
        # Index of the first parent to mate.
        parent1_idx = k%parents.shape[0]
        # Index of the second parent to mate.
        parent2_idx = (k+1)%parents.shape[0]
        # The new offspring will have its first half of its genes taken from the first parent.
        offspring[k, 0:crossover_point] = parents[parent1_idx, 0:crossover_point]
        # The new offspring will have its second half of its genes taken from the second parent.
        offspring[k, crossover_point:] = parents[parent2_idx, crossover_point:]
    return offspring

def mutation(offspring_crossover):
    # Mutation changes a single gene in each offspring randomly.
    for idx in range(offspring_crossover.shape[0]):
        # The random value to be added to the gene.
        random_value = numpy.random.uniform(0.0, 1.0, 1)
        random_index = numpy.random.random_integers(0, 1, 1)
        offspring_crossover[idx, random_index] = offspring_crossover[idx, random_index] + random_value
    return offspring_crossover
def getaccuracy(gamma,c):
    #gamma is a parameter for non linear hyperplanes. The higher the gamma value it tries to exactly fit the training data set
    #c is the penalty parameter of the error term. It controls the trade off between smooth decision boundary and 
    #classifying the training points correctly.
    
    train_X,test_X,train_Y,test_Y=getData()
    svclassifier = SVC(kernel='rbf',gamma=gamma,C=c)
    svclassifier.fit(train_X,train_Y)
    pred=svclassifier.predict(test_X)
    accuracy=svclassifier.score(test_X,test_Y)
    return accuracy