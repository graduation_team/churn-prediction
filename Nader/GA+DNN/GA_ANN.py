import numpy
import sys
sys.path.append("..")
from ipynb.fs.full.data_preprocessing import *
import tensorflow as tf
if tf.test.gpu_device_name():
    print('Default GPU Device: {}'.format(tf.test.gpu_device_name()))
else:
    print("Please install GPU version of TF")
import keras
from keras.models import Sequential #used to init. neural network
from keras.layers import Dense #used to create the layers in neural network
from keras.layers import Dropout
from sklearn.metrics import confusion_matrix


def cal_pop_fitness(pop):
    # Calculating the fitness value of each solution in the current population.
    # The fitness function calulates the sum of products between each input and its corresponding weight.
    fitness=[]
    for i in range(len(pop)):
        fitness.append(getaccuracy(int(abs(pop[i][0])),int(abs(pop[i][1])),int(abs(pop[i][2]))))
    fitness_np = numpy.array(fitness)
    return fitness_np

def select_mating_pool(pop, fitness, num_parents):
    # Selecting the best individuals in the current generation as parents for producing the offspring of the next generation.
    parents = numpy.empty((num_parents, pop.shape[1]))
    for parent_num in range(num_parents):
        max_fitness_idx = numpy.where(fitness == numpy.max(fitness))
        max_fitness_idx = max_fitness_idx[0][0]
        parents[parent_num, :] = pop[max_fitness_idx, :]
        fitness[max_fitness_idx] = -99999999999
    return parents

def crossover(parents, offspring_size):
    offspring = numpy.empty(offspring_size)
    # The point at which crossover takes place between two parents. Usually, it is at the center.
    crossover_point = numpy.uint8(offspring_size[1]/2)

    for k in range(offspring_size[0]):
        # Index of the first parent to mate.
        parent1_idx = k%parents.shape[0]
        # Index of the second parent to mate.
        parent2_idx = (k+1)%parents.shape[0]
        # The new offspring will have its first half of its genes taken from the first parent.
        offspring[k, 0:crossover_point] = parents[parent1_idx, 0:crossover_point]
        # The new offspring will have its second half of its genes taken from the second parent.
        offspring[k, crossover_point:] = parents[parent2_idx, crossover_point:]
    return offspring

def mutation(offspring_crossover):
    # Mutation changes a single gene in each offspring randomly.
    for idx in range(offspring_crossover.shape[0]):
        # The random value to be added to the gene.
        random_value = numpy.random.uniform(0.0, 1.0, 1)
        offspring_crossover[idx, 2] = offspring_crossover[idx, 2] + random_value
    return offspring_crossover
def getaccuracy(l1,l2,l3):
    train_X,test_X,train_Y,test_Y=getData()
    classifier=Sequential()
    #classifier.add(Dense(units=100,kernel_initializer='uniform',activation='relu',input_dim=19))
    #classifier.add(Dropout(p=.1))
    #10 due to take an avg num between num of inputs and output (11+1)/2=6
    classifier.add(Dense(units=l1,kernel_initializer='uniform',activation='relu',input_dim=19))
    classifier.add(Dropout(p=.1))
    classifier.add(Dense(units=l2,kernel_initializer='uniform',activation='relu'))
    classifier.add(Dropout(p=.1))
    classifier.add(Dense(units=l3,kernel_initializer='uniform',activation='relu'))
    classifier.add(Dropout(p=.1))
    classifier.add(Dense(units=1,kernel_initializer='uniform',activation='sigmoid'))
    #activation="softmax" is the same as sigmoid but for more than 2 classes and also output_dim=num of outputs
    classifier.compile(optimizer='adam',loss='binary_crossentropy',metrics=['accuracy'])
    classifier.fit(train_X,train_Y,batch_size=10,epochs=2)
    pred=classifier.predict(test_X)
    pred=(pred>0.5)
    cm=confusion_matrix(pred,test_Y)
    accuracy=((cm[0][0]+cm[1][1])/(cm[0][0]+cm[0][1]+cm[1][0]+cm[1][1]))
    return accuracy